# CryptoHog Prototype

> Proof of Concept of an [Binance](https://www.binance.com) [triangular arbitrage](https://www.investopedia.com/terms/t/triangulararbitrage.asp) trading bot.
>
> This bot *proofs* that [triangular arbitrage](https://www.investopedia.com/terms/t/triangulararbitrage.asp) on [Binance](https://www.binance.com) is indeed possible. **BUT** the trading fees of 0.1% (taker / maker), the fact that the bot has to trade three times and thus is prone to [slippage](https://www.investopedia.com/terms/s/slippage.asp) on every trade and the importance of very low latency, made it rather clear that making any *$$* with it, is less certain than dying while playing russian-roulette. So it works, but the conditions have to be perfect. In ~99,9% of the time they are not. You do the math.
>
> **Disclaimer**: The use of this application **is at your own risk**. You are fully liable for any damage or losses it might cause. You have been warned. This application was made for research purposes only, so it is **not** production ready!

## Links

- [Binance](https://www.binance.com)
- [Triangular Arbitrage](https://www.investopedia.com/terms/t/triangulararbitrage.asp)

## License

[MIT](./LICENSE)

## Author

>(c) Jan (impmja) Schulte