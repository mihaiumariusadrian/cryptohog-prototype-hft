# CryptoHog Prototype

## Developing

### Prerequisites

- NodeJS ver. 10.15.0+ (Use nvm!)
- [Node Version Manager](https://github.com/creationix/nvm)
- [Docker-CE](https://docs.docker.com/install/) & [Docker-Compose](https://docs.docker.com/compose/install/)

### Installing / Getting started

```bash
# Activate NodeJS Version defined within the .nvmrc file
$ nvm use

# or

$ nvm use 10.15.0
```

```bash
# Install default dependencies
$ yarn install
```

### Debugging

>This project contains a VSCode launch configuration which allows to execute the Typescript files directly via the Debugger. Just put a Breakpoint where you want and press F5.

### Building

#### Local

```bash
# Build application in development mode
$ yarn build
```

```bash
# Build application in production mode
$ yarn build:prod
```

#### Docker

```bash
# Build the Docker image & start the container / application
$ make up
```

```bash
# Rebuild the Docker image & start the container / application from ground up
$ make rebuild
```

### Linting & Testing
```bash
# Lint the applications source
$ yarn lint
```

```bash
# Run all test solution(s)
$ yarn test
```

```bash
# Continuously run all test solution(s)
$ yarn test:watch
```

### Executing

#### Local
```bash
# Execute the previos built application
$ yarn start
```

#### Docker

```bash
# Starts the Docker container
$ make start
```

```bash
# Stops the Docker container
$ make stop
```

### Shell & Logs

#### Docker

```bash
# Shows the stdout of the Docker container
$ make logs
```

```bash
# Connects to the Docker containers bash shell
$ make bash

# or

$ make shell

# You leave the bash shell with
$ exit
```

## Links

- [Binance](https://www.binance.com)
- [Triangular Arbitrage](https://www.investopedia.com/terms/t/triangulararbitrage.asp)

## License

> [MIT](./LICENSE)

## Author

>(c) Jan Schulte
>
>Date: 2019-02-05 20:15:58