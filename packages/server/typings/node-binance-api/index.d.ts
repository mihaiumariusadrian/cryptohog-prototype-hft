declare module 'node-binance-api' {
  interface BinanceTicker {
    symbol: string;
  }

  type BinanceWebsocketHandle = string;

  interface BinanceMarketDepth {
    asks: { [price: number]: number };
    bids: { [price: number]: number };
    eventTime: number;
  }

  interface BinanceWebsockets {
    subscribe(
      url: string,
      callback: (error: any, data: any) => void,
      reconnect: boolean,
    ): BinanceWebsocketHandle;
    subscribeCombined(
      url: string,
      callback: (error: any, data: any) => void,
      reconnect: boolean,
    ): BinanceWebsocketHandle;
    terminate(handle: BinanceWebsocketHandle): void;
    depth(symbols: string | string[], callback: (depth: any) => void): BinanceWebsocketHandle;
    depthCache(
      symbols: string | string[],
      callback: (symbol: string, depth: BinanceMarketDepth) => void,
      limit?: number,
    ): BinanceWebsocketHandle;
  }

  class BinanceAPI {
    constructor();
    options(opt: any, callback: boolean): void;
    first(object: object): any;
    last(object: object): any;
    sortBids(symbol: number | string | object, max?: number, baseValue?: boolean): any;
    sortAsks(symbol: number | string | object, max?: number, baseValue?: boolean): any;
    prevDay(
      symbols: string | string[] | boolean,
      callback: (error: any, ticker: BinanceTicker[]) => void,
    ): void;
    websockets: BinanceWebsockets;
  }

  export = BinanceAPI;
}
